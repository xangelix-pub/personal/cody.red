#!/usr/bin/env bash
# WARN: MINIMAL RHEL 9.0
# WARN: MUST BE RUN AS ROOT FROM GIT DIR WD
# CLEAN: rm -rf ~/cody.red ~/.cloudflared /root/.cloudflared /etc/cloudflared

cd ~/cody.red
yay
#sudo pacman -S nano git podman podman-docker python39 cockpit cockpit-machines cockpit-pcp cockpit-podman
##podman-compose
#systemctl enable --now cockpit.socket
#systemctl enable --now podman.socket
#runuser -l tux -c  "python -m ensurepip --upgrade && python -m pip install --upgrade pip"
#runuser -l tux -c  "cd ~ && git clone https://github.com/containers/podman-compose.git && cd podman-compose && python setup.py install --user && cd .. && rm -rf podman-compose"
#sysctl -w net.core.rmem_max=2048000

if [[ -d "~/cody.red" ]]; then
  echo "'~/cody.red' exists."
  cd ~/cody.red
  git pull
else
  git clone https://gitlab.com/xangelix-pub/cody.red.git
  cd ~/cody.red
fi

if [[ -f "./cloudflared/cert.pem" ]]; then
  echo "./cloudflared/cert.pem exists."
else
  echo "./cloudflared/cert.pem did not exist."
  yay -S --noconfirm cloudflared
  cloudflared login
  cp ~/.cloudflared/cert.pem ./cloudflared/cert.pem
  rm -rf ~/.cloudflared
  yay -R --noconfirm cloudflared
fi

if ls ./cloudflared/*.json 1> /dev/null 2>&1; then
  echo "./cloudflared/*.json exists."
else
  echo "./cloudflared/*.json did not exist."
  yay -S --noconfirm cloudflared
  mkdir ~/.cloudflared
  cp ./cloudflared/cert.pem ~/.cloudflared/cert.pem
  chown -R $(whoami):$(whoami) ~/.cloudflared
  echo "INPUT: cloudflared.tunnel.name"
  read
  cloudflared tunnel create $REPLY
  cp ~/.cloudflared/*.json ./cloudflared/
  rm -rf ~/.cloudflared
  cloudflared_tunnel_id=$(ls ./cloudflared -l | grep json | cut -d ' ' -f 11 | cut -d '.' -f 1)
  sed -Ei 's/tunnel: "([0-9a-f-]*)"/tunnel: "'"$cloudflared_tunnel_id"'"/' ./cloudflared/config.yml
  sed -Ei 's,credentials-file: "/etc/cloudflared/([0-9a-f-]*).json",credentials-file: "/etc/cloudflared/'"$cloudflared_tunnel_id"'.json",' ./cloudflared/config.yml
  chown -R $(whoami):$(whoami) ~/cody.red
  yay -R --noconfirm cloudflared

  echo "ADD APPS HERE: https://dash.teams.cloudflare.com/"
  read
  echo "ADD DNS HERE: https://dash.cloudflare.com/"
  echo "$cloudflared_tunnel_id.cfargotunnel.com"
  read
fi

# if [[ -f "./nextcloud/cache.env" ]]; then
#   echo "./nextcloud/cache.env exists."
# else
#   echo "SET: REDISPASSWORD and REDIS_HOST_PASSWORD"
#   read
#   echo "REDIS_PASSWORD=$REPLY" >> ./nextcloud/cache.env
#   echo "REDIS_HOST_PASSWORD=$REPLY" >> ./nextcloud/cache.env
# fi

if [[ -f "./nextcloud/db.env" ]]; then
  echo "./nextcloud/db.env exists."
else
  echo "./nextcloud/db.env did not exist."

  echo "SET: POSTGRES_DB"
  #read
  #echo "POSTGRES_DB=$REPLY" >> ./nextcloud/db.env
  echo "POSTGRES_DB=nextcloud" >> ./nextcloud/db.env

  echo "SET: POSTGRES_USER"
  #read
  #echo "POSTGRES_USER=$REPLY" >> ./nextcloud/db.env
  echo "POSTGRES_USER=nextcloud" >> ./nextcloud/db.env

  echo "SET: POSTGRES_PASSWORD"
  read
  echo "POSTGRES_PASSWORD=$REPLY" >> ./nextcloud/db.env
fi

if [[ -f "./nextcloud/app.env" ]]; then
  echo "./nextcloud/app.env exists."
else
  echo "./nextcloud/app.env did not exist."

  echo "SET: PHP_MEMORY_LIMIT"
  #read
  #echo "PHP_MEMORY_LIMIT=$REPLY" >> ./nextcloud/app.env
  echo "PHP_MEMORY_LIMIT=2G" >> ./nextcloud/app.env

  echo "SET: PHP_UPLOAD_LIMIT"
  #read
  #echo "PHP_UPLOAD_LIMIT=$REPLY" >> ./nextcloud/app.env
  echo "PHP_UPLOAD_LIMIT=16G" >> ./nextcloud/app.env


  echo "SET: POSTGRES_HOST"
  #read
  #echo "POSTGRES_HOST=$REPLY" >> ./nextcloud/app.env
  echo "POSTGRES_HOST=nextcloud-db" >> ./nextcloud/app.env

  echo "SET: REDIS_HOST"
  #read
  #echo "REDIS_HOST=$REPLY" >> ./nextcloud/app.env
  echo "REDIS_HOST=nextcloud-cache" >> ./nextcloud/app.env

  echo "SET: OVERWRITEPROTOCOL"
  #read
  #echo "OVERWRITEPROTOCOL=$REPLY" >> ./nextcloud/app.env
  echo "OVERWRITEPROTOCOL=https" >> ./nextcloud/app.env

  echo "SET: OVERWRITECLIURL"
  #read
  #echo "OVERWRITECLIURL=$REPLY" >> ./nextcloud/app.env
  echo "OVERWRITECLIURL=https://next.cody.red" >> ./nextcloud/app.env
fi

echo "./cody-red-media-1.env did not exist."

echo "SET: S3_BUCKET_NAME"
read
echo "S3_BUCKET_NAME=$REPLY" > ./cody-red-media-1.env
s3_bucket_name=$REPLY

echo "SET: S3_ACCESS_KEY"
read
echo "S3_ACCESS_KEY=$REPLY" >> ./cody-red-media-1.env
s3_access_key=$REPLY

echo "SET: S3_SECRET_KEY"
read
echo "S3_SECRET_KEY=$REPLY" >> ./cody-red-media-1.env
s3_secret_key=$REPLY

#   echo "SET: S3_SSE_C"
#   read
#   echo "S3_SSE_C=$REPLY" >> ./cody-red-media-1.env
#   s3_sse_c=$REPLY
echo -e "[default]\naws_access_key_id = $s3_access_key\naws_secret_access_key = $s3_secret_key" > ~/creds
mkdir -p ./media/cody-red-media-1
geesefs --memory-limit 4000 --max-flushers 32 --max-parallel-parts 32 --part-sizes 25 --endpoint "https://s3.wasabisys.com" --shared-config ~/creds --debug --debug_fuse --debug_s3 --dir-mode 0755 --file-mode 0755 --uid 82 --gid 0 -o allow_other cody-red-media-1 ./media/cody-red-media-1

cd ~/cody.red && sudo docker-compose down && sudo docker-compose up -d
