# cody.red

## Setup

1. Set `REDIS_PASSWORD` and `REDIS_HOST_PASSWORD` to the same password in `nextcloud/cache.env`.
2. Set `POSTGRES_PASSWORD` to a password in `nextcloud/db.env`.

## Dev Environment

### Markdown Editor

- [Hotkeys](https://ld246.com/article/1582778815353)
